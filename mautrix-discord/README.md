LUG uses the [Mautrix Discord bridge](https://github.com/mautrix/discord) to connect our Discord server with our Matrix instance at lug#lug.ncsu.edu.

Follow [the bridge setup instructions here](https://docs.mau.fi/bridges/go/setup.html?bridge=discord).

1. Download the binary to `/opt/lug-matrix/mautrix-discord/mautrix-discord`.
2. `cd /opt/lug-matrix/mautrix-discord` and run `./mautrix-discord -g` to generate `config.yaml` and `registration.yaml`. Make the following changes to `config.yaml`, keeping the rest the same:
    ```yaml
    homeserver:
        address: https://lug.ncsu.edu
        domain: lug.ncsu.edu
    database:
        type: postgres
        uri: file:/opt/lug-matrix/mautrix-discord/mautrix-db.sqlite3
    bridge:
        permissions: 
            "*": relay
            "lug.ncsu.edu": user
            "@chika:lug.ncsu.edu": admin
            "chika.chika@matrix.org": admin # personal email: rohanssrao@gmail.com
    ```
3. [Register the bridge with Synapse](https://docs.mau.fi/bridges/general/registering-appservices.html#synapse) by copying `registration.yaml` to `/var/lib/docker/volumes/synapse-data/_data/mautrix-discord-registration.yaml` and modifying `homeserver.yaml` in the same directory like so:
    ```yaml
    app_service_config_files:
    - /data/mautrix-discord-registration.yaml
    ```

4. Restart Synapse: `sudo systemctl restart synapse`.

5. Make a systemd unit for the bridge by creating `/etc/systemd/system/mautrix-discord.service` with this:

    ```ini
    [Unit]                                                           
    Description=Starts mautrix discord bridge                        
                                                                    
    [Service]                                                        
    ExecStart=/bin/sh -c '/opt/lug-matrix/mautrix-discord/mautrix-discord --config /opt/lug-matrix/mautrix-discord/config.yaml'       
    Type=simple                                                      
    RemainAfterExit=yes                                              
    Restart=on-failure                                               
    RestartSec=5s                                                    
                                                                    
    [Install]                                                        
    WantedBy=multi-user.target
    ```

    Then `sudo systemctl start mautrix-discord.service`.

Follow [these instructions](https://docs.mau.fi/bridges/go/discord/authentication.html#bot-token-login) to make a Discord bot that handles bridging.

## Bridging a channel

1. Make sure the Discord bot has read/write access to the channel. At the time of writing, it has the "LUG Matrix Bridge" role that grants it access to all channels.
2. Send a message in the Discord channel. This will trigger the bridge to create a corresponding Matrix room and it will invite the admin to it.
3. Log into Element as the admin, accept the invite, then send `!discord set-pl <admin-username>:lug.ncsu.edu 100` in the Matrix room to give yourself superuser permissions on the Matrix room to change its various settings.
4. Send `!discord set-relay --create` in the Matrix room to create a relay webhook on the Discord channel. This makes messages appear on Discord with the username and avatar of the Matrix user.
5. Open the room settings and edit the room's visibility and who can send messages, if necessary. For example, by default a user shouldn't be able to read or send messages in #admin or send messages in #announcements.
6. Go to the LUG @ NC State space in Element, click Add Existing Room, and add the room to the space.