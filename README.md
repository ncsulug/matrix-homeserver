Documentation for the LUG's Matrix homeserver and Matrix-Discord bridge. Files are in `/opt/lug-matrix` on char.csc.ncsu.edu.

[Homeserver documentation](matrix-homeserver/README.md)

[Bridge documentation](mautrix-discord/README.md)