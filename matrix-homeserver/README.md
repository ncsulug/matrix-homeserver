This directory houses configuration and installation instructions for the Linux Users' Group's Synapse homeserver for Matrix at lug.ncsu.edu. Documentation: https://hub.docker.com/r/matrixdotorg/synapse

# Prerequisites

- docker and docker-compose (available via apt)
- nginx configuration serving https://lug.ncsu.edu

# Installation

1. Add the following to the second server block within `/etc/nginx/sites-available/lug.ncsu.edu` to forward Matrix requests to the right places:
    ```nginx
    # Matrix
    location ~ ^(/_matrix|/_synapse/client) {
        # note: do not add a path (even a single /) after the port in `proxy_pass`,
        # otherwise nginx will canonicalise the URI and cause signature verification
        # errors.
        proxy_pass http://localhost:8008;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $host;

        # Nginx by default only allows file uploads up to 1M in size
        # Increase client_max_body_size to match max_upload_size defined in homeserver.yaml
        client_max_body_size 50M;
    
    # Synapse responses may be chunked, which is an HTTP/1.1 feature.
    proxy_http_version 1.1;
    }

    # Delegation of federation traffic (how Matrix servers find and interact with each other from the default port (8448) to 443.
    # This is done because at the time of writing, a firewall blocks all ports but 80 and 443 for our server.
    location /.well-known/matrix/server {
        return 200 '{"m.server": "lug.ncsu.edu:443"}';
    }
    location /.well-known/matrix/client {
        return 200 '{"m.homeserver": {"base_url": "https://lug.ncsu.edu"}}';
    }
    ```
    Documentation: https://matrix-org.github.io/synapse/latest/reverse_proxy.html#nginx
1. Run `sudo systemctl restart nginx` to apply these changes.
1. Generate the config file at `/var/lib/docker/volumes/synapse-data/_data/homeserver.yaml` by running this:
    ```bash
    docker run -it --rm \
    --mount type=volume,src=synapse-data,dst=/data \
    -e SYNAPSE_SERVER_NAME=lug.ncsu.edu \
    -e SYNAPSE_REPORT_STATS=yes \
    matrixdotorg/synapse:latest generate
    ```

# Operation

While in this directory, run the server in the background with `docker-compose up -d`.
You can follow logs with `docker logs -f synapse`.

Restart with:
```sh
sudo docker compose -f /opt/lug-matrix/matrix-homeserver/docker-compose.yml up -d
```